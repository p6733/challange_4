package com.example.demo.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class FilmControlTest {
    @Autowired
    private FilmControl filmControl;
    @Test
    void addFilm() {
        String respon=filmControl.addFilm("kukira rumah","tayang");
        assertEquals("add film sukses",respon);
    }

    @Test
    void updateFilm() {
        filmControl.updateFilm(1,"ternyata rumah","tidaktayang");
    }

    @Test
    void deleteFilmByCode(){
        filmControl.deleteFilmByCode(1);
    }

    @Test
    void showFilm() {
        filmControl.showFilm();
    }
    @Test
    void showFilmScheduleByName(){
        filmControl.showFilmByName("kukira rumah");
    }

    @Test
    void showFilmTayang(){
        filmControl.showFilmByTayang();
    }

}