package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;
import sun.util.calendar.BaseCalendar;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Setter
@Getter
@Entity(name = "Schedules")
public class Schedules implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "schedule_id")
    private Integer scheduleID;
    @Column(name = "harga_tiket")
    private Integer hargaTiket;


    @Column(name = "tanggal_tayang")
    private  String tanggalTayang;

    @Column(name = "jam_mulai")
    private String jamMulai;
    @Column(name = "jam_selesai")
    private String jamSelesai;

    @ManyToOne
    @JoinColumn(name = "film_code")
    private Films FilmCode;


}
