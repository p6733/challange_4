package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;
@Setter
@Getter
@Embeddable
public class SeatId implements Serializable {
    @Column(name ="studio_name" )
    private String studioName;
    @Column(name ="nomor_kursi" )
    private Integer nomorKursi;
}
