package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
public class Seats {
    @EmbeddedId
    private SeatId seatId;
}
