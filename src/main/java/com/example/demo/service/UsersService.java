package com.example.demo.service;

import com.example.demo.model.Users;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UsersService {
    public void saveUser(String username, String password, String email);
    public void updateUser(String username, String password, String email,Integer userId);
    public void deleteUsers(Integer userId);
    public List<Users> userList();
}
