package com.example.demo.service;

import com.example.demo.model.Films;
import com.example.demo.model.Schedules;
import com.example.demo.repository.FilmsRepository;
import com.example.demo.repository.SchedulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class FilmsServiceImpl implements FilmsService,SchedulesService{
    @Autowired
    FilmsRepository filmsRepository;
    @Autowired
    SchedulesRepository schedulesRepository;
    @Override
    public void saveFilm(String filmName, String sedangTayangAtauTidak) {
        Films films = new Films();
        films.setFilmName(filmName);
        films.setSedangTayangAtauTidak(sedangTayangAtauTidak);
        filmsRepository.save(films);
    }

    @Override
    public void updateFilm(Integer filmCode, String filmName, String sedangTayangAtauTidak) {
        filmsRepository.updateFilm(filmCode,filmName,sedangTayangAtauTidak);
    }
    public void deleteFilm(Integer filmCode){
        filmsRepository.deleteFilm(filmCode);
    }





    @Override
    public List<Films> filmsList() {
        return filmsRepository.findAll();
    }

    @Override
    public List<Schedules> getByNamaFilm(String filmName) {
       return schedulesRepository.findByNamaFilm(filmName);
    }

    @Override
    public List<Schedules> getByTayang(String SedangTayangAtatuTidak) {
        return schedulesRepository.findByTayang(SedangTayangAtatuTidak);
    }
}
