package com.example.demo.service;

import com.example.demo.model.Schedules;

import java.util.List;

public interface SchedulesService {
    public List<Schedules> getByNamaFilm(String filmName);
    public List<Schedules>getByTayang(String SedangTayangAtatuTidak);
}
