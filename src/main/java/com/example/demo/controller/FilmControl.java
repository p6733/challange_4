package com.example.demo.controller;

import com.example.demo.model.Films;
import com.example.demo.model.Schedules;
import com.example.demo.service.FilmsService;
import com.example.demo.service.SchedulesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class FilmControl {
    @Autowired
    FilmsService filmsService;

    @Autowired
    SchedulesService schedulesService;

    public String addFilm(String filmName, String sedangTayangAtautidak){
        filmsService.saveFilm(filmName,sedangTayangAtautidak);
        return "add film sukses";

    }
    public void updateFilm(Integer filmCode,String filmName, String sedangTayangAtautidak){
        filmsService.updateFilm(filmCode,filmName,sedangTayangAtautidak);
    }
    public void deleteFilmByCode(Integer filmCode){
        filmsService.deleteFilm(filmCode);
    }



    public void showFilm(){
        List<Films> filmsList = filmsService.filmsList();
        System.out.println("Code Film\t\tNama Film\t\tStatus");
        filmsList.forEach(films -> {
            if(films.getSedangTayangAtauTidak().equals("tayang")){
                System.out.println(films.getFilmCode()+"\t\t\t"+films.getFilmName()+
                        "\t\t\t"+films.getSedangTayangAtauTidak());
            }
        });
    }
    public List<Schedules> showFilmByName(String filmName){
        List<Schedules> filmSchedule=schedulesService.getByNamaFilm(filmName);
        System.out.println("Nama Film\t\tTanggal Tayang\t\tJam Mulai\t\tJam Selesai\t\tHarga Tiket");
        filmSchedule.forEach(Films->{
            System.out.println(Films.getFilmCode().getFilmName()+"\t\t\t"+Films.getTanggalTayang()+"\t\t\t"+Films.getJamMulai()+
                    "\t\t\t"+Films.getJamSelesai()+"\t\t\tRp. "+Films.getHargaTiket());
                }
        );
        return filmSchedule;
    }
    public List<Schedules> showFilmByTayang(){
        List<Schedules>filmSchedule=schedulesService.getByTayang("tayang");
        System.out.println("Nama Film\t\tTanggal Tayang\t\tJam Mulai\t\tJam Selesai\t\tHarga Tiket");
        filmSchedule.forEach(Films->{
                    System.out.println(Films.getFilmCode().getFilmName()+"\t\t\t"+Films.getTanggalTayang()+"\t\t\t"+Films.getJamMulai()+
                            "\t\t\t"+Films.getJamSelesai()+"\t\t\tRp. "+Films.getHargaTiket());
                }
        );
        return filmSchedule;

    }
}
