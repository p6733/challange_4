package com.example.demo.controller;

import com.example.demo.model.Users;
import com.example.demo.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class UsersControl {
    @Autowired
    private UsersService usersService;
    public String userAdd(String username, String password, String email){
        usersService.saveUser(username,password,email);
        return "penambahan berhasil";
    }
    public String updateUser(String username,  String password,String email, Integer userId){
        usersService.updateUser(username,password,email,userId);
        return "update berhasil";
    }
    public String deleteUser(Integer userId){
        usersService.deleteUsers(userId);
        return "delete berhasil";
    }
    public  void  showUser(){
        List<Users> usersList = usersService.userList();
        System.out.println("userId\tusername\t\tpassword\t\t\t\temail");
        usersList.forEach(users -> {
            System.out.println(users.getUserId()+"\t\t"+users.getUsername()+
                    "\t\t\t\t"+users.getPassword()+"\t\t"+users.getEmail());
        });
    }

}
